const express = require('express');

var app = express();


// userModel must live outside the get response
// empty object where { "name": name, "email": email}
  var userModel = {}; 

// alternatively just use email as key and check if key exists is TRUE 
// var emails = {};
// if (emails[email.toLowerCase()]){
//   // failure response 
// }



// routing
app.use(express.static(__dirname + '/public'));
app.use("/libs", express.static(__dirname + "/bower_components"));

app.get('/register-interest',function(req, res){
  // initialize 
  var userName = req.query.name;
  var userEmail = req.query.email; 
  var registered = false; 
  var userId;
  var lastUserId = Object.keys(userModel).length;
  // check presence 
  if (lastUserId !== 0) {
    for (var i in userModel) {
      if ((userModel[i]["email"]) === userEmail) {
        registered = true;
      } 
    }
  }
  // choose response
  if (registered) {
    errorResponse(res);
    return; // breaks out of entire response function
  }
  userId = lastUserId + 1;
  userModel[userId] = {
    "name" : userName,
    "email" : userEmail 
  };
  successResponse(res, userId, userName, userEmail);
});

// callbacks 
  function successResponse(res, userId, userName, userEmail) {
    res.status(200);
    res.type('application/json');
    res.json({ 
      "userId" : userId,
      "name" : userName,
      "email" : userEmail
    });
  }

  function errorResponse(res) {
    res.status(400);
    res.type('application/json');
    res.json({ "error" : "You have already registered with us" });
  }


// ports and listen 
var port = process.argv[2] || 3000;

app.listen(port, function(){
  console.log("Now listening on port %d", port);
});