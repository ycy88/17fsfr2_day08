(function(){
  angular
    .module("RegApp", [])
    .controller("RegC", RegC)
  
  RegC.$inject = [ "$http" ];

  // create controller to be hoisted 
  function RegC(httpService) {
    var regC = this;

    regC.clearForm = function(){
      regC.name = "";
      regC.email = "";
      regC.result = "";
    };
    //initialize 
    regC.clearForm();

    regC.submitForm = function(){
      // promise don't need to know server and port number, assume same host 
      httpService.get('/register-interest', {
        params: {
          "name" : regC.name,
          "email" : regC.email.toLowerCase()
        }
      })
      //success 200, note that you can chain the catch behind the first function
      .then(function(result){
        console.log(result.data.name);
        regC.result = "Hi " + result.data.name
                      + ", you have registered with ID " + result.data.userId
                      + " and email " + result.data.email;
      })
      //failure 400 
      .catch(function(result){
        console.log(result.data.error);
        regC.result = result.data.error + ". Please use another email";
      });
    }// end submitForm

  }// end controller 


})();